" K-DR channel approximation derived from Lyle_KO.pas [Borg-Graham'99] "
import math

def E_exp(dt, tau):
    if tau > 0:
        return 1 - math.exp(-dt / tau)
    else:
        return 1

"{---------------- K - DR -----------------}"
def MyK_tau_inf(v2):
    # Eq. for 'n'
    a = 0.1 * math.exp( (v2 + 25) / 7)
    b = 0.1 * math.exp(-(v2 + 25) / 7)
    tau_n = 1 / (a + b) + 2
    n_inf = a / (a + b)
    return tau_n, n_inf

def MyK_conductance(x, y, z):
    return x * y

def MyK_current(V, gK, VK, nn, dt):
    tau_n, n_inf = MyK_tau_inf(V)
    nn = nn + E_exp(dt, tau_n) * (n_inf - nn)
    yK = 0.47
    g = gK * MyK_conductance(nn, yK, 0)
    return g * (V - VK), nn, g

def MyK_init(V):
    tau1, nn = MyK_tau_inf(V)
    return nn

if __name__ == '__main__':
    nn=MyK_init(60)
    print(nn)