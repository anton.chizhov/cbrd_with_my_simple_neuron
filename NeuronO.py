import copy

from My_NaO import MyNa_h_inf, MyNa_conductance, MyNa_current, MyNa_init
from My_KslowO import MyKslow_tau_inf, MyKslow_conductance, MyKslow_current, MyKslow_init
from My_KO import MyK_tau_inf, MyK_conductance, MyK_current, MyK_init
#import numpy

#{ Parameters: }
class NeuronProperties:
    dT_AP: float
    FixThr: float
    Vreset: float
    gK: float
    VK: float
    gKA: float
    VKA: float
    gNa: float
    VNa: float
    tau_mm: float
    gL: float
    VL: float
    Square: float
    tau_m0: float
    C_membr: float
    If_CBRD: int
    If_K_Na_currents: int

#{ Variables: }
class NeuronVariables:
    V: float
    DVDt: float
    nn: float
    nA: float
    mm: float
    hh: float
    ii: float
    VT: float
    tLastSpike: float
    gActive: float
    Vold: float
    DVDt_old: float

#{==================================================================}
class TMyNrn:
    def __init__(self, NP_):
        # Properties:
        self.NP = copy.deepcopy(NP_)
        # Variables:
        self.NV = NeuronVariables()

    def InitialConditions(self,Stim):
        v2_ = 0.0
        self.NV.V = self.NP.VL + Stim.I0 / (self.NP.Square * 1e6) / self.NP.gL
        self.NV.DVDt = 0.0
        self.NV.DVDt_old = self.NV.DVDt
        self.NV.Vold = self.NV.V
        v2_ = self.NV.V
        self.NV.nn = MyK_init(v2_)
        self.NV.nA = MyKslow_init(v2_)
        self.NV.VT, self.NV.mm, self.NV.hh, self.NV.ii, self.NV.tLastSpike = MyNa_init(v2_)
        self.NV.tLastSpike = -888
        self.NV.gActive = 0

    #{-----------------------------------------------------------------------}
    def MembranePotential(self, uu, ss, t, dt):
    # {-----------------------------------------------------------------------}
        # Initialize currents
        Isyn = -ss * (self.NV.V - self.NP.VL) + uu
        if self.NP.If_K_Na_currents == 1:
            # Calculate ion currents
            IK_,     self.NV.nn, gK_     = MyK_current(self.NV.V, self.NP.gK, self.NP.VK, self.NV.nn, dt)
            IKslow_, self.NV.nA, gKslow_ = MyKslow_current(self.NV.V, self.NP.gKA, self.NP.VKA, self.NV.nA, dt)
            INa_, self.NV.VT, self.NV.mm, self.NV.hh, self.NV.ii, self.NV.tLastSpike, gNa_ = \
                MyNa_current((self.NP.If_CBRD == 0), self.NV.V,
                                      self.NP.gNa, self.NP.VNa, self.NP.tau_mm, self.NV.mm,
                                      self.NV.hh, self.NV.ii, self.NV.tLastSpike, t, dt)
            self.NV.gActive = gK_ + gKslow_ + gNa_
        else:
            IK_ = 0
            IKslow_ = 0
            INa_ = 0
            self.NV.gActive = 0

        # Update voltage
        self.NV.Vold = self.NV.V
        self.NV.DVDt_old = self.NV.DVDt
        self.NV.DVDt = 1 / self.NP.C_membr * (-self.NP.gL * (self.NV.V - self.NP.VL) - IK_ - IKslow_ - INa_ + Isyn)
        # ***************************************
        self.NV.V = self.NV.V + dt * self.NV.DVDt
        # ***************************************

    # Check for spikes
        if (self.NP.If_K_Na_currents == 0) and (self.NP.If_CBRD == 0) and (self.NV.V > self.NP.VL + self.NP.FixThr):
            self.NV.V = self.NP.Vreset
            self.NV.DVDt = 0
            self.NV.tLastSpike = t


# if __name__ == '__main__':
#     from Slice import TParameters, TStim, DefaultParameters
#
#     Pars, Stim, NP0 = DefaultParameters()
#     n=TMyNrn(NP0)
#     n.InitialConditions(Stim)
#     n.MembranePotential(Stim.I,Stim.G,0,Pars.dt)
#     print(Pars.dt)
