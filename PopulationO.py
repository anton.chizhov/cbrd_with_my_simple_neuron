import copy

from NeuronO import TMyNrn, NeuronProperties
import math
import numpy as np


class PopulationProperties:
    Name: str
    NP: NeuronProperties
    dts: float
    ts_end: float
    sgm_V: float
    Nts: int
    taum_taus: float

class TPopulation:
    def __init__(self, PP_,ANrn):
        MaxPh = PP_.Nts+1
        # Control variables
        self.Isyn = 0.0
        self.Gsyn = 0.0
        # Variables
        self.nu = 0.0
        self.U = 0.0
        self.dUdt = 0.0
        self.Bum = 0.0
        self.tBum = 0.0
        self.ro = [0.0] * MaxPh
        self.ts = [0.0] * MaxPh
        self.VT = [0.0] * MaxPh
        self.Hzrd = [0.0] * MaxPh
        self.AB = [0.0] * MaxPh
        # Fulfil parameters
        self.PP = copy.deepcopy(PP_)
        # Create set of neurons
        self.Nrn = [TMyNrn] * MaxPh

        for i in range(PP_.Nts + 1):
            self.Nrn[i] = TMyNrn(PP_.NP)
            self.Nrn[i].NP.If_CBRD = 1


    def nts_by_ts(self, ts):
        """Find particle just behind ts or precisely at ts"""
        if self.PP.Nts == 0:
            return 0
        t2 = 1e8
        i2 = -1
        for i in range(self.PP.Nts + 1):
            if self.ts[i] >= ts and self.ts[i] < t2:
                i2 = i
                t2 = self.ts[i]
        # Exception
        if i2 == -1:
            i2 = self.PP.Nts
        return i2

    def xThr(self):
        x = 0
        StH = 0
        SH = 0
        for i in range(self.PP.Nts + 1):
            StH += self.ts[i] * self.Hzrd[i] * self.PP.dts
            SH += self.Hzrd[i] * self.PP.dts
        if SH > 1e-8:
            x = self.nts_by_ts(StH/SH)
        else:
            x = self.PP.Nts
        return x

    def InitialConditions(self, Stim, dt):
        # Set conditions at rest
        for i in range(self.PP.Nts + 1):
            self.Nrn[i].InitialConditions(Stim)
            self.Nrn[i].NP.FixThr = self.Nrn[i].NP.FixThr  # {!!!!}-PP.sgm_V;  {!}

        self.PP.NP = self.Nrn[self.PP.Nts].NP

        # Set conditions at spike
        for i in range(self.PP.Nts):   # except the last one
            self.ConditionsAtSpike(self.Nrn[i])

        # Integrate till "ts"
        self.ts[0] = 0
        self.ts[self.PP.Nts] = self.PP.Nts * self.PP.dts
        for i in range(1, self.PP.Nts):    # except 0 and the last ones
            self.ts[i] = i * self.PP.dts
            nt_ = 0
            while nt_ < self.ts[i] / dt:
                nt_ += 1
                # ******* One step of integration **
                if nt_ * dt > self.PP.NP.dT_AP:
                    self.Nrn[i].MembranePotential(0, 0, self.ts[i], dt)
                # **********************************

        # Fulfil density
        for i in range(self.PP.Nts + 1):
            self.ro[i] = 0
        self.ro[self.PP.Nts] = 1 / self.PP.dts * 1e3  # dts is in ms, ro is in Hz

        # Output variables
        self.nu = 0
        self.U = self.PP.NP.VL
        self.dUdt = 0

    def ConditionsAtSpike(self, ANrn_):
        if ANrn_.NP.If_K_Na_currents == 0:
            ANrn_.NV.V = ANrn_.NP.Vreset
            ANrn_.NV.nn = 0
        else:
            # Conditions for "My" channels
            ANrn_.NV.V = ANrn_.NV.VT
            xThr_ = self.xThr()  # age of the most spiking neurons
            ANrn_.NV.ii = self.Nrn[xThr_].NV.ii
            ANrn_.NV.nn = self.Nrn[xThr_].NV.nn  # or ANrn_.NP.nn_reset
            ANrn_.NV.nA = self.Nrn[xThr_].NV.nA
            ANrn_.NV.mm = 1
            ANrn_.NV.hh = 0
        ANrn_.NV.DVDt = 0

    def MembranePotential(self, t, dt):
        for i in range(self.PP.Nts + 1):
            if self.ts[i] > self.PP.NP.dT_AP:
                self.Nrn[i].MembranePotential(self.Isyn, self.Gsyn, t, dt)
            if i < self.PP.Nts:
                self.ts[i] += dt

        for i in range(self.PP.Nts):      # except the last one
            if self.ts[i] >= self.PP.ts_end:
                self.ts[i] -= self.PP.ts_end
                self.ConditionsAtSpike(self.Nrn[i])
                self.ro[self.PP.Nts] += self.ro[i]
                if t == self.tBum:
                    self.tBum = -self.tBum
                self.ro[i] = self.Bum / (t - self.tBum) * 1e3
                self.tBum = t
                self.Bum = 0

        self.U = self.Nrn[self.PP.Nts].NV.V
        self.dUdt = self.Nrn[self.PP.Nts].NV.DVDt


    # { NUMERICAL METHOD.
    #   Left cell at ts=0:       ro=nu,      U=Vreset, cell size - (t-tBum).
    #   Right cell at ts=ts_end: ro=ro[Nts], U=U[Nts], cell size - dts.
    #   Next to right cell i: ro=ro[i], U=U[i], cell size - ts_end-ts[i]=dts+(t-tBum)
    # }

    def Density(self, dt):
        def F_tilde(x):
            if x > 4:
                return x * math.sqrt(2)
            elif x < -5:
                return 0
            else:
                return math.sqrt(2 / math.pi) * math.exp(-x * x) / (1 - math.erf(x))

        def fA_T(T, taum_taus):
            global exp_Int_0_u
            if abs(T) <= 2.5:
                T3 = T * T * T
                A0 = math.exp(-2.478E-3 - 1.123 * T - 0.2375 * T * T - 0.06567 * T3
                              - 0.01813 * T3 * T - 1.713E-003 * T3 * T * T + 5.484E-004 * T3 * T3
                              + 1.034E-004 * T3 * T3 * T)
            elif T > 2.5:
                A0 = 0
            else:
                A0 = 7
            if taum_taus > 0 and A0 > 0:
                z = A0 * (1 - (1 + taum_taus) ** (-0.71 + 0.0825 * (T + 3)))
            else:
                z = A0
            return max(z, 0)

        g0_ = self.PP.NP.C_membr / self.PP.NP.tau_m0  # mS/cm^2
        sgmV_ = self.PP.sgm_V
        S = 0
        for i in range(self.PP.Nts, -1, -1):  # loop for phase
            dVdt = self.Nrn[i].NV.DVDt
            if self.PP.NP.If_K_Na_currents == 1:
                self.VT[i] = self.Nrn[i].NV.VT - self.Nrn[i].NP.VL
            else:
                self.VT[i] = self.PP.NP.FixThr
            g_g0 = (self.PP.NP.gL + self.Nrn[i].NV.gActive + self.Gsyn) / g0_
            taum_ = self.PP.NP.tau_m0 / g_g0
            V_ = (self.Nrn[i].NV.V - self.PP.NP.VL - self.VT[i]) / (sgmV_ * np.sqrt(2)) * np.sqrt(g_g0)  # sqrt is introduced on 25.02.2017, eq. from p.164
            # Derived from "frozen" Gauss
            if dVdt < 0:
                B = 0
            else:
                B = F_tilde(V_) / sgmV_ * dVdt * 1e3 * np.sqrt(g_g0)  # Hz  # sqrt is introduced on 25.02.2017, eq. from p.164
            # From self-similar Fokker-Planck
            if (self.ts[i] < 8) and (dVdt <= 0):
                A = 0
            else:
                A = fA_T(-V_, self.PP.taum_taus) / taum_ * 1e3  # Hz
            # Spike half-duration
            if self.ts[i] <= self.PP.NP.dT_AP:
                B = 0
                A = 0
            if (self.PP.NP.If_K_Na_currents == 1) and (self.Nrn[i].NV.hh < 0.5):
                B = 0
                A = 0
            # *** Source ***
            dro = min(self.ro[i], self.ro[i] * (1 - np.exp(-(B + A) * dt / 1e3)))
            # ***************************
            self.ro[i] = self.ro[i] - dro
            # ***************************
            self.Hzrd[i] = dro / dt * 1e3
            self.AB[i] = A + B
            S = S + dro
        self.nu = S * self.PP.dts / dt
        self.Bum = self.Bum + S * self.PP.dts / 1e3
        return self.nu

def CreatePopulationBy_NP_O(Name, Nts, ts_end, sgm_V, taum_taus, NP,ANrn):
    # Create Object-Population
    PP_ = PopulationProperties()
    PP_.Name = Name
    PP_.NP = NP
    PP_.Nts = Nts
    PP_.ts_end = ts_end
    PP_.dts = ts_end / Nts
    PP_.sgm_V = sgm_V
    PP_.taum_taus = taum_taus
    APop = TPopulation(PP_,ANrn)
    return APop

#if __name__ == '__main__':
    #from Slice import TParameters, TStim, DefaultParameters

    # Pars, Stim, NP0 = DefaultParameters()
    # t_arr, nu_arr, V_arr, mm_arr, hh_arr, nn_arr, nA_arr = CBRD(NP0, Stim, Pars)
