
from NeuronO import TMyNrn, NeuronProperties
from PopulationO import CreatePopulationBy_NP_O
import random
import math
import numpy as np
import matplotlib.pyplot as plt


class TParameters:
    # Time
    t_end: float
    dt: float
    nt_end: int
    # Phase space and intrinsic noise
    Nts: int
    ts_end: float
    sgm_V: float
    Repr_sgm_V: float
    taum_taus: float
    # Other
    n_show: int
    n_DrawPhase: int
    n_write: int

class TStim:
    I: float
    G: float
    Freq: float
    Duration: float
    t_start: float
    I0: float

def DefaultParameters():
    Pars = TParameters()
    # Phase space and intrinsic noise
    Pars.Nts = 100
    Pars.ts_end = 100 # ms
    Pars.sgm_V = 3 # mV
    Pars.Repr_sgm_V = 3 # sgm_V[1]
    Pars.taum_taus = 0

    # Neuron parameters
    NP0 = NeuronProperties()
    NP0.If_CBRD = 0
    NP0.If_K_Na_currents = 1
    NP0.C_membr = 0.7 # muF/cm^2
    NP0.VL = -65 # mV
    NP0.tau_m0 = 14.4 # ms
    NP0.Square = 1e-4 # cm^2
    NP0.gL = NP0.C_membr/NP0.tau_m0 # mS/cm^2
    NP0.FixThr = 10 # mV
    NP0.dT_AP = 0
    NP0.Vreset = NP0.VL
    NP0.VK = -80 # mV
    NP0.VKA = -80 # mV
    NP0.VNa = 55 # mV
    NP0.gK = 2 # mS/cm^2
    NP0.gKA = 0.5 # mS/cm^2
    NP0.gNa = 2 # mS/cm^2
    NP0.tau_mm = 7 # ms

    # Stimul
    Stim = TStim()
    Stim.G = 0
    Stim.Freq = 0 # Hz
    Stim.I = 300 # pA
    Stim.Duration = 200 # ms
    Stim.t_start = 20 # ms
    Stim.I0 = 0

    # Time
    Pars.t_end = 250 # ms
    Pars.dt = 0.15 # ms
    Pars.nt_end = int(Pars.t_end/Pars.dt)

    # Other parameters
    Pars.n_show = 2
    Pars.n_DrawPhase = 200
    Pars.n_write = 100

    return Pars, Stim, NP0

def Current_Iind(t, Stim, NP0, Pars):
    I_ind_per_cm2 = Stim.I / (NP0.Square * 1e6)  # muA/cm^2
    if t > Pars.dt and t > Stim.t_start and t <= Stim.t_start + Stim.Duration:
        if Stim.Freq == 0:  # constant
            MeanCurr = I_ind_per_cm2
        elif Stim.Freq < 0:  # sinus with increasing freq.
            MeanCurr = I_ind_per_cm2 * math.sin(2 * math.pi * (abs(Stim.Freq) * t / 1e3 / 10) * t)
        else:  # sinus
            MeanCurr = I_ind_per_cm2 * math.sin(2 * math.pi * Stim.Freq * t / 1e3)
    else:
        MeanCurr = Stim.I0 / (NP0.Square * 1e6)  # muA/cm^2
    return MeanCurr


def Conductance_Gind(t, Stim, NP0, Pars):
    if t > Pars.dt and t > Stim.t_start and t <= Stim.t_start+Stim.Duration:
        Conductance_Gind = Stim.G/(NP0.Square*1e6) # mS/cm^2
        if Stim.Freq < 0: # sinus with increasing freq.
            Conductance_Gind = Stim.G/(NP0.Square*1e6) * abs(math.sin(2*math.pi*abs(Stim.Freq/3)*t/1e3/10*t))
    else:
        Conductance_Gind = 0
    return Conductance_Gind


def SingleNeuron(NP0, Stim, Pars):
    # Create representative neuron
    ANrn = TMyNrn(NP0)
    ANrn.NP = NP0
    ANrn.InitialConditions(Stim)
    t_arr = np.linspace(0, 1, Pars.nt_end+1)
    V_arr = np.linspace(ANrn.NV.V, ANrn.NV.V, Pars.nt_end+1)
    mm_arr = np.linspace(0,0, Pars.nt_end+1)
    hh_arr = np.linspace(0,0, Pars.nt_end+1)
    nn_arr = np.linspace(0,0, Pars.nt_end+1)
    nA_arr = np.linspace(0,0, Pars.nt_end+1)
    t = 0
    nt = 0
    while nt < Pars.nt_end:
        # ----- Step -----
        t = t + Pars.dt
        nt = nt + 1
        # Representative neuron
        Noise_ = Pars.Repr_sgm_V * np.sqrt(2 / Pars.dt * NP0.tau_m0) * NP0.gL * random.gauss(0, 1)  # muA/cm^2
        uu_ = Current_Iind(t, Stim, NP0, Pars) + Noise_
        ss_ = Conductance_Gind(t, Stim, NP0, Pars)
        #*******************************************
        ANrn.MembranePotential(uu_, ss_, t, Pars.dt)
        #*******************************************
        V_arr[nt] = ANrn.NV.V
        mm_arr[nt] = ANrn.NV.mm
        hh_arr[nt] = ANrn.NV.hh
        nn_arr[nt] = ANrn.NV.nn
        nA_arr[nt] = ANrn.NV.nA
        t_arr[nt] = t

    return t_arr, V_arr, mm_arr, hh_arr, nn_arr, nA_arr


def MonteCarlo(NP0, Stim, Pars, t_end, N_Iterations):
    maxNSpikes = int(t_end)
    iMC = 0
    Spikes = np.linspace(0,0, maxNSpikes+1)
    tMC_arr = np.linspace(0,maxNSpikes, maxNSpikes+1)
    # Iterations
    while True:
        iMC = iMC + 1
        # Create representative neuron
        ANrn = TMyNrn(NP0)
        ANrn.NP = NP0
        ANrn.InitialConditions(Stim)
        t = 0
        nt = 0
        # Loop for time steps
        while True:
            # ----- Step -----
            t = t + Pars.dt
            nt = nt + 1
            # Representative neuron
            Noise_ = Pars.Repr_sgm_V * np.sqrt(2 / Pars.dt * NP0.tau_m0) * NP0.gL * random.gauss(0, 1)  # muA/cm^2
            uu_ = Current_Iind(t, Stim, NP0, Pars) + Noise_
            ss_ = Conductance_Gind(t, Stim, NP0, Pars)
            # *******************************************
            ANrn.MembranePotential(uu_, ss_, t, Pars.dt)
            # *******************************************

            # Count spikes
            bin = int(t)  # each ms
            if abs(ANrn.NV.tLastSpike - t) < Pars.dt / 2:
                Spikes[bin] += 1  # spikes in ms

            if nt >= Pars.nt_end or bin >= maxNSpikes:
                break # end of time loop
        if iMC == N_Iterations:
            break # end of iterations

    return tMC_arr, Spikes/iMC*1000, iMC

#{-----------------------------------------------------------------------}
def CBRD(NP0, Stim, Pars, t_plot):
    "Simulates CBRD model for LIF and conductance-based neurons."

    # Create representative neuron
    ANrn = TMyNrn(NP0)
    ANrn.NP = NP0
    ANrn.InitialConditions(Stim)

    # Create and initiate Population
    Pop = CreatePopulationBy_NP_O('P', Pars.Nts, Pars.ts_end, Pars.sgm_V, Pars.taum_taus, NP0,ANrn)
    Pop.InitialConditions(Stim, Pars.dt)

    # Arrays to plot
    t_arr  = np.linspace(0, 1, Pars.nt_end+1)
    nu_arr = np.linspace(0, 0, Pars.nt_end+1)
    V_arr = np.linspace(ANrn.NV.V, ANrn.NV.V, Pars.nt_end+1)
    mm_arr = np.linspace(0,0, Pars.nt_end+1)
    hh_arr = np.linspace(0,0, Pars.nt_end+1)
    nn_arr = np.linspace(0,0, Pars.nt_end+1)
    U_arr = np.linspace(ANrn.NV.V, ANrn.NV.V, Pars.nt_end+1)
    Isyn_arr = np.linspace(0,0, Pars.nt_end+1)
    Gsyn_arr = np.linspace(0,0, Pars.nt_end+1)
    ro_ts_arr = np.zeros((len(t_plot),Pars.Nts+1))
    ts_arr = np.zeros((len(t_plot),Pars.Nts+1))

    t = 0
    nt = 0
    while nt < Pars.nt_end:
        # ----- Step -----
        t += Pars.dt
        nt += 1

        # ***********************
        Pop.Density(Pars.dt)
        # ***********************

        Pop.Isyn = Current_Iind(t, Stim, NP0, Pars)
        Pop.Gsyn = Conductance_Gind(t, Stim, NP0, Pars)

        # ************************************
        Pop.MembranePotential(t, Pars.dt)
        # ************************************

        # Representative neuron
        Noise_ = Pars.Repr_sgm_V * np.sqrt(2 / Pars.dt * NP0.tau_m0) * NP0.gL * random.gauss(0, 1)  # muA/cm^2
        ANrn.MembranePotential(Pop.Isyn + Noise_, Pop.Gsyn, t, Pars.dt)
        # *************
        t_arr[nt] = t
        V_arr[nt] = ANrn.NV.V
        mm_arr[nt] = ANrn.NV.mm
        hh_arr[nt] = ANrn.NV.hh
        nn_arr[nt] = ANrn.NV.nn
        nA_arr[nt] = ANrn.NV.nA
        nu_arr[nt] = Pop.nu
        U_arr[nt] = Pop.U
        Isyn_arr[nt] = Pop.Isyn * (NP0.Square * 1e6)
        Gsyn_arr[nt] = Pop.Gsyn / NP0.gL
        for i in range(len(t_plot)):
            if np.abs(t-t_plot[i])<Pars.dt/2:
                ro_ts_arr[i,:] = Pop.ro[:]
                ts_arr[i, :] = Pop.ts[:]

    return t_arr, nu_arr, V_arr, mm_arr, hh_arr, nn_arr, nA_arr, U_arr, Isyn_arr, Gsyn_arr, ro_ts_arr, ts_arr

if __name__ == '__main__':

    Pars, Stim, NP0 = DefaultParameters()
    t_plot=[70,100,150,200]   # time moments to plot distributions in t*-space
    #                                                      *********************************************
    t_arr,         V_arr, mm_arr, hh_arr, nn_arr, nA_arr = SingleNeuron(NP0, Stim, Pars)
    tMC_arr, nuMC_arr, iMC                               = MonteCarlo(NP0, Stim, Pars, Pars.t_end, 1000)
    t_arr, nu_arr, V_arr, mm_arr, hh_arr, nn_arr, nA_arr, U_arr, Isyn_arr, Gsyn_arr, ro_ts_arr, ts_arr\
                                                         = CBRD(NP0, Stim, Pars, t_plot)
    #                                                      *********************************************

    # Plotting
    fig, axes = plt.subplots(nrows=3, ncols=2)
    axes[0,0].plot(t_arr, V_arr, "-b", label="V", linewidth=1)
    axes[0,0].plot(t_arr, U_arr, "-r", label="U(t)", linewidth=2)
    axes[0,0].plot(t_arr, t_arr*0+NP0.FixThr, "-g", label="VT", linewidth=2)
    axes[0,1].plot(t_arr, mm_arr, "-r", label="mm", linewidth=1)
    axes[1,0].plot(t_arr, hh_arr, "-y", label="hh", linewidth=1)
    axes[1,1].plot(t_arr, nn_arr, "-g", label="nn", linewidth=1)
    axes[1,1].plot(t_arr, nA_arr, "-r", label="nA", linewidth=1)
    axes[2,0].plot(tMC_arr, nuMC_arr, "-r", label="nu from MC", linewidth=1)
    axes[2,0].plot(t_arr, nu_arr, "-b", label="nu from CBRD", linewidth=1)
    for i in range(len(t_plot)):
        axes[2,1].plot(ts_arr[i,:Pars.Nts], ro_ts_arr[i,:Pars.Nts], ".", label="ro("+str(t_plot[i])+",t*)", linewidth=1)
    axes[0,0].legend()
    axes[0,1].legend()
    axes[1,0].legend()
    axes[1,1].legend()
    axes[2,0].legend()
    axes[2,1].legend()
    plt.xlabel("t*")
    plt.show()

    # Writing
    yyy = open('t_I_G_U_nu.dat', 'w')
    rrr = open('Repr_t_V_a.dat', 'w')
    for i in range(len(t_arr)):
        # Writing
        yyy.write('{:8.3f} {:8.5f} {:8.5f} {:8.3f}\n'.format(t_arr[i], Isyn_arr[i], Gsyn_arr[i], U_arr[i], nu_arr[i]))
        rrr.write('{:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f}\n'.format(t_arr[i], V_arr[i], mm_arr[i], hh_arr[i], nn_arr[i], nA_arr[i]))
    yyy.close()
    rrr.close()
    # Writing from Monte-Carlo
    s1 = 'MC_{:03d}.dat'.format(round(iMC))
    bbb = open(s1, 'w')
    for i in range(len(tMC_arr)):
        bbb.write('{:8.3f} {:8.3f}\n'.format(tMC_arr[i], nuMC_arr[i]))
    bbb.close()