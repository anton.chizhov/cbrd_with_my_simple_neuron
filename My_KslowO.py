" Derived from Lyle_KA.pas "
import math

def E_exp(dt, tau):
    if tau > 0:
        return 1 - math.exp(-dt / tau)
    else:
        return 1

"{---------------- KA -----------------}"
def MyKslow_tau_inf(v2):
    # Eq. for 'nA'
    a = 5 * math.exp(v2/4)
    b = 0.05
    tau_n = 1 / (a + b) + 4
    n_inf = a / (a + b)
    return tau_n, n_inf

def MyKslow_conductance(x, y, z):
    return x * y

def MyKslow_current(v2, gKA, VKA, nA, dt):
    tau_n, n_inf = MyKslow_tau_inf(v2)
    nA = nA + E_exp(dt, tau_n) * (n_inf - nA)
    g = gKA * MyKslow_conductance(nA, 1, 0)
    return g * (v2 - VKA), nA, g

def MyKslow_init(V):
    tau1, nA = MyKslow_tau_inf(V)
    return nA
