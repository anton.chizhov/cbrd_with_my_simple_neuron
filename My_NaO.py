" Approximation of Na(V) channels with threshold-linear approximation "
import math

def E_exp(dt, tau):
    if tau > 0:
        return 1 - math.exp(-dt / tau)
    else:
        return 1

"{---------------- Na -----------------}"
def MyNa_h_inf(v2):
    return 1 / (1 + math.exp((v2 + 44) / 4))  # Milescu, Fig4, inactivation curve

def MyNa_conductance(x, y, z):
    return x**2 * z

def MyNa_current(IfApplyConditionsAtSpike, v2, gNa, VNa, tau_mm, mm, hh, ii, tLastSpike, t, dt):
    v2_ = -44 + 4 * math.log(1/ii - 1)  # reverse function of h_inf
    VT = -51 + (v2_ / 5 + 12)**2  # Variant 7' - providing VT > v2 for very large currents
    if IfApplyConditionsAtSpike and v2 > VT and hh > 0.5:
        tLastSpike = t
        mm = 1
        hh = 0
    mm = mm + dt / tau_mm * (0 - mm)
    h_inf = MyNa_h_inf(v2)
    hh = hh + dt / 10 * (h_inf - hh)
    g = gNa * MyNa_conductance(mm, 0, ii)
    MyNa_Current = g * (v2 - VNa)
    ii = ii + E_exp(dt, 40) * (MyNa_h_inf(v2) - ii)
    return MyNa_Current, VT, mm, hh, ii, tLastSpike, g

def MyNa_init(V):
    VT = -50  # mV
    tLastSpike = -888  # ms
    mm = 0
    hh = 1
    ii = MyNa_h_inf(V)
    return VT, mm, hh, ii, tLastSpike
